<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {
        $title = "Welcome To Laravel ";
        return view('pages.index')->with('title', $title);
    }

    public function about() {
        $title = "About The App";
        return view('pages.about')->with('title', $title);
    }

    public function services() {
        $data = array(
            'title' => 'Services',
            'services' => ['1- Register Yourself ','2- Post something','3- You can browse & read member posts without an account',
             '4- Comment on posts']
        );
        return view('pages.services')->with($data);
    }
}
