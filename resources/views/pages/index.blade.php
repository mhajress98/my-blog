@extends('layouts.app')

@section('title')
    Home
@endsection

@section('content')
    <div class="jumbotron text-center">
    <h1>Laravel Blog</h1>
    <p>My first laravel app</p>
    @if(Auth::guest())
    <p><a class="btn btn-primary btn-lg" href="{{ route('login') }}">Login</a>
       <a class="btn btn-success btn-lg" href="{{ route('register') }}">Register</a></p>
       @else
            <h4>Welcome</h4>
    @endif
    </div>
@endsection

