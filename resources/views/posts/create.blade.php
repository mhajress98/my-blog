@extends('layouts.app')

@section('title')
    Create Post
@endsection

@section('content')
    <h1>Create Post</h1>
    {!! Form::open(['action' => 'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
                {!! Form::label('title', 'Post Title') !!}
                {!! Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title']) !!}
        </div>
        <div class="form-group">
                {!! Form::label('body', 'Content') !!}
                {!! Form::textarea('body', '', ['id' => 'editor', 'class' => 'form-control', 'placeholder' => 'Write Something']) !!}
        </div>
        <div class="form-group">
            {!! Form::file('cover_image')!!}
        </div>
        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}

        <div id="editor"></div>
        <script>var editor = new Jodit('#editor');</script>
@endsection
