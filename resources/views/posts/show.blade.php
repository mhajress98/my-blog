@extends('layouts.app')

@section('title')
    {{$post->title}}
@endsection

@section('content')
    <a href="{{ route('post_index')}}" class="btn btn-outline-secondary" style="
    margin-bottom: 10px;">Go Back</a>
    <hr>
    <h1>{{$post->title}}</h1>
    <hr>
    <div class="container">
        <img style="width:100%" src="{{asset('storage/cover_images').'/'.$post->cover_image}}" style="background-size:cover; width: 1100px; height:500px;">
    </div>
    <br>
    <div class="card card-body bg-light" style="
    margin-top: 10px;">
        {!!$post->body!!}
    </div>
    <hr>
    <small>Written on {{ $post->created_at->toDateString()}} By {{$post->user->name}}</small>
    <hr>

    @if(!Auth::guest())
        @if(Auth::user()->id == $post->user_id)
            <a href="{{$post->id}}/edit" class="btn btn-outline-secondary">Edit</a>
            {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'float-right','onsubmit' => 'return confirm("Delete Post ?")']) !!}
            {!! Form::hidden('_method', 'DELETE') !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
            <hr>
        @endif
    @endif

<h3>Comments</h3>
    <hr>
    @if (count($comments) > 0)
    <ul class="list-group">
        @foreach ($comments as $comment)
            <li class="list-group-item"><Strong>{{$post->user->name}} : </Strong><hr>{{$comment->comment_body}}</li>
            <hr>
        @endforeach
    </ul>
        @else
        <ul class="list-group">
            <li class="list-group-item">No Comments !</li>
        </ul>
    @endif
    <hr>
    @if(!Auth::guest())
    {!! Form::open(['action' => 'CommentsController@store', 'method' => 'POST']) !!}
        {!! Form::hidden('post_id', $post->id) !!} {{-- Pass Post_Id in the $request parameter to the @store function --}}
        <div class="form-group">
            {!! Form::label('comment', 'Comment Something') !!}
            {!! Form::textarea('comment', '', [ 'class' => 'form-control', 'placeholder' => 'Write Here']) !!}
        </div>
        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
    @endif


@endsection
