@extends('layouts.app')

@section('title')
    Edit Post
@endsection

@section('content')
    <h1>Edit Post</h1>
    {!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
                {!! Form::label('title', 'Post Title') !!}
                {!! Form::text('title', $post->title, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
        </div>
        <div class="form-group">
                {!! Form::label('body', 'Content') !!}
                {!! Form::textarea('body', $post->body, ['id' => 'editor', 'class' => 'form-control', 'placeholder' => 'Write Something']) !!}
        </div>
        <div class="form-group">
                {!! Form::file('cover_image')!!}
            </div>
        {!! Form::hidden('_method', 'PUT') !!}
        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}

        <div id="editor"></div>
        <script>var editor = new Jodit('#editor');</script>
@endsection
