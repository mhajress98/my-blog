@extends('layouts.app')

@section('title')
    Index
@endsection


@section('content')
    <h1>Posts</h1>
    @if (count($posts) > 0)
        @foreach ($posts as $post)
            <div class="card card-body bg-light" style="
            margin-top: 10px;">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <img style="width:100%" src="storage/cover_images/{{$post->cover_image}}" alt="{{$post->title}}">
                    </div>
                        <div class="col-md-8 col-sm-8">
                            <h3><a href="posts/{{$post->id}}">{{$post->title}}</a></h3>
                            <small>Written on {{date('d-m-Y', strtotime($post->created_at))}} By {{$post->user->name}} </small>
                        </div>
                    </div>
                {{-- <p>{{$post->body}}</p> --}}
                </div>
        @endforeach
        {{$posts->links()}} {{-- pagination --}}
    @else
        <p>No Posts found !</p>
    @endif
@endsection
