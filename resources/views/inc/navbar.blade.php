      <nav class="navbar navbar-expand-md navbar-light" style="background-color: #e3f2fd;">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                    <a class="nav-link" href="{{ route('home') }}">Home</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" href="{{ route('about') }}">About</a>
                                  </li>
                                  <li class="nav-item">
                                          <a class="nav-link" href="{{ route('services') }}">Services</a>
                                  </li>
                                  <li class="nav-item">
                                          <a class="nav-link" href="{{ route('post_index')}}">Blog</a>
                                  </li>

                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="/blog/public/dashboard" >
                                    {{ Auth::user()->name }} {{--<span class="caret"></span>--}}
                                </a>
                            </li>
                                {{-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                </div> --}}
                                <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       || Logout ||
                                    </a>
                                </li>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                            <li class="nav-item"><a class="nav-link" href="{{ route('posts.create') }}">Create Post</a></li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
