@extends('layouts.app')

@section('title')
    Dashboard
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a class="btn btn-primary" href="{{ route('posts.create') }}">Create Post</a>
                    <hr>
                        <h3>Your Blog Posts</h3>
                        @if(count($posts) > 0)
                    <table class="table table-striped">
                        <tr>
                            <th>Title</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach ($posts as $post)
                        <tr>
                                <td>{{$post->title}}</td>
                                <td><a href="posts/{{$post->id}}/edit" class="btn btn-outline-secondary">Edit</a></td>
                                <td>
                                        {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'float-right', 'onsubmit' => 'return confirm("Delete Post ?")']) !!}
                                        {!! Form::hidden('_method', 'DELETE') !!}
                                        {!! Form::submit('Delete', ['class' => 'btn-danger btn float-right']) !!}
                                </td>
                            </tr>
                        @endforeach
                        </table>
                        @else
                            <p>You Have no posts yet .. write some </p>
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
