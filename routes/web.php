<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('home');

Route::get('/about', 'PagesController@about')->name('about');

Route::get('/services', 'PagesController@services')->name('services');

Route::resource('posts', 'PostsController');

Route::get('post_index', 'PostsController@index')->name('post_index');

// Route::get('posts/{post}/comment', 'CommentsController@store')->name('create.comment');

Route::resource('comments', 'CommentsController');
// Route::post('comments/{id}', 'CommentsController@store');

/*Route::get('/users/{user}/{id}', function($user, $id){
    return 'user  '. $user .'  has id of ' .$id;
});*/

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');
